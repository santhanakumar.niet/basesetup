import  axios from "axios";

const client =  axios.create({
    baseURL: 'https://some-domain.com/api/',
    timeout: 3000,
    headers: {'X-Custom-Header': 'foobar'}
  });

export default client;
