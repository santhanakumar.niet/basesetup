import React from "react";
import Loadable from "react-loadable";
import IconButton from '@material-ui/core/IconButton';

const loading = () => null;

const routes = [
  {
    path: "/",
    component: Loadable({
      loader: () => import("../containers/Home"),
      loading,
    }),
    isPrivate: true,
  },
  // {
  //   path: "/transaction-summary/:customerId",
  //   component: Loadable({
  //     loader: () => import("containers/TransactionSummary"),
  //     loading,
  //   }),
  //   isPrivate: true,
  //   key: "customer-transaction-summary",
  // },
  // {
  //   path: "/transaction-summary",
  //   component: Loadable({
  //     loader: () => import("containers/TransactionSummary"),
  //     loading,
  //   }),
  //   isPrivate: true,
  //   key: "transaction-summary",
  // },
  // {
  //   path: "/remittance",
  //   component: Loadable({
  //     loader: () => import("containers/RemittancePage"),
  //     loading,
  //   }),
  //   isPrivate: true,
  //   isRedirect: true,
  // },
  // {
  //   path: "/remittance/new",
  //   component: Loadable({
  //     loader: () => import("containers/RemittancePage"),
  //     loading,
  //   }),
  //   isPrivate: true,
  // },
  // {
  //   path: "/remittance/:id",
  //   component: Loadable({
  //     loader: () => import("containers/RemittancePage"),
  //     loading,
  //   }),
  //   isPrivate: true,
  // },
  // {
  //   path: "/customers/:term?",
  //   component: Loadable({
  //     loader: () => import("containers/CustomerSearch"),
  //     loading,
  //   }),
  //   isPrivate: true,
  // },
  // {
  //   path: "/manage-beneficiary",
  //   component: Loadable({
  //     loader: () => import("containers/ManageBeneficiary"),
  //     loading,
  //   }),
  //   isPrivate: true,
  // },
  // {
  //   path: "/contact-us",
  //   component: Loadable({
  //     loader: () => import("containers/ContactUs"),
  //     loading,
  //   }),
  //   isPrivate: true,
  // },
  // {
  //   path: "/login",
  //   component: Loadable({
  //     loader: () => import("containers/LoginPage"),
  //     loading,
  //   }),
  // },
  {
    path: "/",
    component: Loadable({
      loader: () => import("../containers/Home"),
      loading,
    }),
    exact: false,
  },
];

export default routes;
