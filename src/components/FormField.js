import React from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  error: {
    fontSize: 12,
    color: "#ff0000",
  },
});

const FormField = ({
  classes,
  fieldComponent: FieldComponent,
  error,
  ...rest
}) => {
  return (
    <div>
      <FieldComponent {...rest} />
      {error && <span className={classes.error}>{error}</span>}
    </div>
  );
};

export default withStyles(styles)(FormField);
