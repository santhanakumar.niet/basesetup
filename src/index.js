import React from 'react';
import ReactDOM from 'react-dom';

//standard imports
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import createHistory from "history/createBrowserHistory";


import App from './containers/App';
import configureStore from "./configureStore";

import * as serviceWorker from './serviceWorker';

const initialState= {};
const history = createHistory();
const store = configureStore(initialState, history);

const render = Component => {
    return ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Component/>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root')
    )
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

render(App);

if (module.hot) {
    module.hot.accept('./containers/App', () => {
        const NextApp = require('./containers/App').default;
        render(NextApp);
    });
}
