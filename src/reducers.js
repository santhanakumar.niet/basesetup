import { combineReducers } from "redux";
import { reducer as reduxFormReducer } from "redux-form";

import { connectRouter } from 'connected-react-router'

export default (history) => combineReducers({
  form: reduxFormReducer,
  router: connectRouter(history)
});
