import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
// import { Notifications } from "finablr-ui";
import { Grow } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles/style";
import { hideNotification } from "./reducer";

class Notification extends Component {
  handleClose = id => () => {
    this.props.dispatch(hideNotification(id));
  };
  render() {
    const { notifications, classes } = this.props;
    // if (notifications.length === 0) {
      return null;
    // }

    // return (
    //   <div
    //     className={classes.containerNotification}
    //     id="notificaitons-container"
    //   >
    //     {notifications.map(
    //       ({
    //         key,
    //         heading,
    //         content,
    //         umStyle = "success",
    //         delayHide = 5000,
    //       }) => {
    //         return (
    //           <Grow
    //             key={key}
    //             in={true}
    //             style={{ transformOrigin: "90% 100% 0" }}
    //           >
    //             <div>
    //               {/* <Notifications
    //                 umStyle={umStyle}
    //                 placement="bottom-right"
    //                 onClose={this.handleClose(key)}
    //                 umClass={classes.removePosition}
    //                 delayHide={delayHide}
    //               >
    //                 <div className={classes.notification}>
    //                   {heading !== "" && <h3>{heading}</h3>}
    //                   {content}
    //                 </div>
    //               </Notifications> */}
    //             </div>
    //           </Grow>
    //         );
    //       },
    //     )}
    //   </div>
    // );
  }
}

// const MapStateToProps = ({ notifications: { notifications } }) => {
//   return {
//     notifications,
//   };
// };

export default compose(
  // connect(MapStateToProps),
  withStyles(styles),
)(Notification);
