const styles = theme => ({
  notification: {
    "& h3": {
      margin: "0 0 15px",
      fontSize: "16px",
    },
    "& p": {
      margin: "0 0 5px",
    },
    "& p:last-child": {
      margin: 0,
    },
  },
  containerNotification: {
    position: "fixed",
    bottom: 20,
    right: 20,
    zIndex: 1600,
  },
  removePosition: {
    position: "relative !important",
    marginTop: 10,
  },
});

export default styles;
