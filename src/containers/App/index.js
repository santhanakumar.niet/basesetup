/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";

// import Header from "containers/Header";
import Notification from "../Notification";
import Loader from "../PageLoader";

import Auth from "../../components/Auth";
import Router from "../../components/Router";
import routes from "../../routes";

import styles from "./styles/style";

const App = props => {
  return (
    <Fragment>
      <CssBaseline />
      <div>
        {/* {Auth.isAuthenticated() && <Header />} */}
        <Router routes={routes} />
        <Notification />
      </div>
      <Loader />
    </Fragment>
  );
};

const MapStateToProps = state => {
  return {
    data: state.userInfo,
    language: state.language,
  };
};
export default compose(
  withRouter,
  connect(MapStateToProps),
  withStyles(styles),
)(App);
